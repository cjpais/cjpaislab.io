---
title: "Deploying This Website"
date: 2020-01-12T19:48:42-08:00
draft: false
tags: ["website", "documentation"]
description: "test"
summary: "Quick notes on how this website was deployed"
---

- Realized I need to get the site going or I will spin forever trying to make things perfect
- This is not perfection. This will grow
    - One of the first things will be to integrate CI with Twitter 
    - Will want some kind of iOS app to add notes/etc. Central place to post stuff
- Using Hugo as the static site generator
- Using [Hugo Notepadium](https://themes.gohugo.io/hugo-notepadium/) theme for now
- Everything hosted on GitLab and published via CI
    1. [Installed Hugo](https://gohugo.io/getting-started/installing/) on my Mac `brew install hugo`
    2. Created repo `cjpais.gitlab.io`
    3. Cloned repo on my PC `git clone git@gitlab.com:cjpais/cjpais.gitlab.io.git`
    4. Create site inside repo forcefully `hugo new site cjpais.gitlab.io --force`
    5. Add theme 
        - `git submodule add https://github.com/cntrump/hugo-notepadium.git themes/hugo-notepadium`
        - Edit `config.toml` and add `theme = "hugo-notepadium"`
    6. Follow rest of [instructions](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) for CI on GitLab
    7. Forward domain cjpais.com
    8. Wait some minutes before the site is deployed. Mine 404'd for a while
